import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created by Alex on 23-May-16.
 */
public class TestSchedule extends BaseTest{
    SchedulePage schedulePage;
    LoginPage loginPage;
    HospitalsPage hospitalsPage;
    DepartmentsPage departmentsPage;
    DoctorsPage doctorsPage;


    @BeforeMethod
    public void beforeMethod() {
        super.beforeMethod();
        loginPage = PageFactory.initElements(browserActions.getDriver(), LoginPage.class);
        schedulePage = PageFactory.initElements(browserActions.getDriver(), SchedulePage.class);
        hospitalsPage = PageFactory.initElements(browserActions.getDriver(), HospitalsPage.class);
        departmentsPage = PageFactory.initElements(browserActions.getDriver(), DepartmentsPage.class);
        doctorsPage = PageFactory.initElements(browserActions.getDriver(), DoctorsPage.class);
    }

    @Test
    public void checkElementsOnLoginPage() {
        browserActions.goTo(homeUrl);
        loginPage.loginButton.click();
        Assert.assertTrue(browserActions.isElementPresent(loginPage.loginButton), "Login button isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(loginPage.emailField), "Email field isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(loginPage.passwordField), "Password field isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(loginPage.loginSubmitButton), "Submit button isn't present!");
    }

    @Test
    public void checkElementsOnSchedulePage() {
        browserActions.goTo(homeUrl);
        loginPage.logInAction(managerLogin, managerPassword);
        browserActions.goTo(doctorUrl);
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.eventBody), "Event body isn't present!");
        schedulePage.events.get(0).click();
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.eventDetails), "Event details button isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.eventEdit), "Event edit button isn't present!");
        schedulePage.eventEdit.click();
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.editorField), "Editor field isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.saveChanges), "Save changes button isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.cancelChanges), "Cancel changes button isn't present!");
        schedulePage.cancelChanges.click();
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.eventDelete), "Event delete button isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.previousDate), "Previous dave button isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.nextDate), "Next date button isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.switchViewToDay), "Switch view to day button isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.switchViewToMonth), "Switch view to month button isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.switchViewToWeek), "Switch view to week button isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.datePicker), "Date picker button isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.selectToday), "Select today button isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.selectedDate), "Selected date field isn't present!");
        schedulePage.eventDetails.click();
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.detailedEditorField), "Detailed editor field isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.timePeriodHoursStart), "Start hours dropdown isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.timePeriodHoursEnd), "End hours dropdown isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.timePeriodDayStart), "Start day dropdown isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.timePeriodDayEnd), "End day dropdown isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.timePeriodMonthStart), "Start month dropdown isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.timePeriodMonthEnd), "End month dropdown isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.timePeriodYearStart), "Start year dropdown isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.timePeriodYearEnd), "End year dropdown isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.cancelDetailedChanges), "Cancel detailed changes button isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.deleteDetailedChanges), "Delete detailed changes button isn't present!");
        Assert.assertTrue(browserActions.isElementPresent(schedulePage.saveDetailedChanges), "Save detailed changes button isn't present!");
    }

    @Test (dependsOnMethods = "checkElementsOnSchedulePage")
    public void selectSecondHospital() {
        browserActions.goTo(hospitalsUrl);
        System.out.println(hospitalsPage.hospitals.size());
        hospitalsPage.hospitals.get(0).click();
    }

    @Test (dependsOnMethods = "checkElementsOnSchedulePage")
    public void selectSecondDepartment() {
        browserActions.goTo(departmentUrl);
        System.out.println(departmentsPage.departments.size());
        departmentsPage.departments.get(1).click();
    }

    @Test (dependsOnMethods = "checkElementsOnSchedulePage")
    public void selectSecondDoctor() {
        browserActions.goTo(doctorsUrl);
        System.out.println(doctorsPage.doctors.size());
    }

    @Test
    public void clickOnSomething() {
        browserActions.goTo(doctorUrl);
        browserActions.moveToElement(schedulePage.eventBody, 50, 50);
        browserActions.sleep(5);
    }

    @Test (priority = 4, dependsOnMethods = "checkElementsOnSchedulePage")
    public void testIfManagerCanEdit() {
        browserActions.goTo(homeUrl);
        loginPage.logInAction(managerLogin, managerPassword);
        hospitalsPage.hospitals.get(0).click();
        departmentsPage.departments.get(0).click();
        doctorsPage.doctors.get(3).click();
        schedulePage.previousDate.click();
        schedulePage.events.get(6).click();
        schedulePage.eventEdit.click();
        schedulePage.editSchedule("text field test, manager edit, #7");
        schedulePage.saveChanges.click();
    }

    @Test (priority = 5, dependsOnMethods = {"checkElementsOnSchedulePage", "checkElementsOnLoginPage"})
    public void testIfPatientCanEdit() {
        browserActions.goTo(homeUrl);
        loginPage.logInAction(patientLogin, patientPassword);
        browserActions.goTo(hospitalsUrl);
        hospitalsPage.hospitals.get(0).click();
        departmentsPage.departments.get(0).click();
        doctorsPage.doctors.get(3).click();
        schedulePage.previousDate.click();
        schedulePage.events.get(6).click();
        schedulePage.eventEdit.click();
        schedulePage.editSchedule("text field test, patient edit, #7");
        schedulePage.saveChanges.click();
    }

    @Test (priority = 6, dependsOnMethods = "checkElementsOnSchedulePage")
    public void testIfDoctorCanEdit() {
        browserActions.goTo(homeUrl);
        loginPage.logInAction(doctorLogin, doctorPassword);
        browserActions.goTo(hospitalsUrl);
        hospitalsPage.hospitals.get(0).click();
        departmentsPage.departments.get(0).click();
        doctorsPage.doctors.get(3).click();
        schedulePage.previousDate.click();
        schedulePage.events.get(6).click();
        schedulePage.eventEdit.click();
        schedulePage.editSchedule("text field test, doctor edit, #7");
        schedulePage.saveChanges.click();
    }

    @Test (priority = 7, dependsOnMethods = "checkElementsOnSchedulePage")
    public void testIfAdminCanEdit() {
        browserActions.goTo(homeUrl);
        loginPage.logInAction(adminLogin, adminPassword);
        browserActions.goTo(hospitalsUrl);
        hospitalsPage.hospitals.get(0).click();
        departmentsPage.departments.get(0).click();
        doctorsPage.doctors.get(3).click();
        schedulePage.previousDate.click();
        schedulePage.events.get(6).click();
        schedulePage.eventEdit.click();
        schedulePage.editSchedule("text field test, admin edit, #7");
        schedulePage.saveChanges.click();
    }

    @Test (priority = 5)
    public void testManagerEditTimePeriod() {
        browserActions.goTo(homeUrl);
        loginPage.logInAction(managerLogin, managerPassword);
        hospitalsPage.hospitals.get(0).click();
        departmentsPage.departments.get(0).click();
        doctorsPage.doctors.get(3).click();
        schedulePage.previousDate.click();
        schedulePage.events.get(8).click();
        schedulePage.eventDetails.click();
        browserActions.selectTime(schedulePage.timePeriodHoursStart, "12:55");
        browserActions.selectTime(schedulePage.timePeriodHoursEnd, "19:55");
        schedulePage.saveDetailedChanges.click();
    }

    @AfterMethod
    public void afterMethod() {
        browserActions.driver.quit();
    }
}

//https://github.com/github/maven-plugins
//https://github.com/kevinsawicki/github-maven-example
//http://blog.sonatype.com/2009/09/maven-tips-and-tricks-using-github/