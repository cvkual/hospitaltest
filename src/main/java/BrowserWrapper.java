import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by Alex on 25-May-16.
 */
public class BrowserWrapper {

    protected WebDriver driver;

    public BrowserWrapper(WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void goTo(String url) {
        driver.get(url);
    }

    public void sleep(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public List<WebElement> findElementsByXpath(String elements) {
        return driver.findElements(By.xpath(elements));
    }

    public List<WebElement> findElementsByCss(String elements) {
        return driver.findElements(By.cssSelector(elements));
    }

    public String getTitle() {
        return driver.getTitle();
    }

    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    public boolean isElementPresent(WebElement element) {
        try {
            return element.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void browserBack() {
        driver.navigate().back();
    }

    public void browserForward() {
        driver.navigate().forward();
    }

    public void selectTime(WebElement element, String hours) {
        Select dropdown = new Select(element);
        dropdown.selectByVisibleText(hours);
    }

    public void moveToElement(WebElement element, int x, int y) {
        Actions builder = new Actions(driver);
        builder.moveToElement(element, x, y).doubleClick().build().perform();
    }
}
