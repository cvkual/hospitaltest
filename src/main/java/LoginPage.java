import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class LoginPage{

    @FindBy(xpath = "/html/body/header/div/div/div[2]/nav/div/div[2]/ul/li[7]/a/span")
    public WebElement loginButton;

    @FindBy(xpath = "/html/body/header/div/div/div[2]/nav/div/div[2]/ul/li[7]/ul/div/form/div[1]/input")
    public WebElement emailField;

    @FindBy(xpath = "/html/body/header/div/div/div[2]/nav/div/div[2]/ul/li[7]/ul/div/form/div[2]/input")
    public WebElement passwordField;

    @FindBy(id = "login-submit")
    public WebElement loginSubmitButton;

    public void logInAction(String username, String password) {
        loginButton.click();
        emailField.sendKeys(username);
        passwordField.sendKeys(password);
        loginSubmitButton.click();
    }
}